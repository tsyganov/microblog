class MicropostsController < ApplicationController
  before_action :authenticate_user!, only: :create

  def index
    @microposts = Micropost.order(created_at: :desc)
    @micropost = Micropost.new
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      redirect_to root_url
    else
      render 'index'
    end
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content)
  end
end
